/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import Entity.ModoPago;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author erick.gomezusam
 */
@Stateless
public class ModoPagoFacade extends AbstractFacade<ModoPago> implements ModoPagoFacadeLocal {

    @PersistenceContext(unitName = "practica3webPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ModoPagoFacade() {
        super(ModoPago.class);
    }
    
}
