/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Entity.ModoPago;
import ejb.ModoPagoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author erick.gomezusam
 */
@Named(value = "modoPago")
@ManagedBean
@SessionScoped
public class ModoPagoController implements Serializable {

    @EJB
    private ModoPagoFacadeLocal modoPagoFacade;
    private List<ModoPago> listaModoPago;
    private ModoPago modoPago;
    String mensaje="";

    public List<ModoPago> getListaModoPago() {
        this.listaModoPago=modoPagoFacade.findAll();
        return listaModoPago;
    }

    public void setListaModoPago(List<ModoPago> listaModoPago) {
        this.listaModoPago = listaModoPago;
    }

    public ModoPago getModoPago() {
        return modoPago;
    }

    public void setModoPago(ModoPago modoPago) {
        this.modoPago = modoPago;
    }
    
    @PostConstruct
    public void init(){
        this.modoPago=new ModoPago();
    }
    public void crear(){
        try {
            this.modoPagoFacade.create(modoPago);
            this.mensaje = "Datos creados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editar(){
        try {
            this.modoPagoFacade.edit(modoPago);
            this.mensaje = "Datos editados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void eliminar(ModoPago mp){
        try {
            this.modoPagoFacade.remove(mp);
            this.mensaje = "Datos eliminados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void cargarModoPago(ModoPago mp){
        this.modoPago=mp;
    }
}
