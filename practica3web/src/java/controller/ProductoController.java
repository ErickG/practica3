/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Entity.Categoria;
import Entity.Producto;
import ejb.ProductoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author erick.gomezusam
 */
@Named(value = "producto")
@ManagedBean
@SessionScoped
public class ProductoController implements Serializable {

    @EJB
    private ProductoFacadeLocal productoFacade;
    private List<Producto> listaProducto;
    private Producto producto;
    private Categoria categoria;
    String mensaje="";

    public List<Producto> getListaProducto() {
        this.listaProducto=productoFacade.findAll();
        return listaProducto;
    }

    public void setListaProducto(List<Producto> listaProducto) {
        this.listaProducto = listaProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    

    @PostConstruct
    public void init(){
        this.producto=new Producto();
        this.categoria=new Categoria();
    }
    public void crear(){
        try {
            this.producto.setIdCategoria(categoria);
            this.productoFacade.create(producto);
            this.mensaje = "Datos creados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editar(){
        try {
            this.producto.setIdCategoria(categoria);
            this.productoFacade.edit(producto);
            this.mensaje = "Datos editados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void eliminar(Producto p){
        try {
            this.productoFacade.remove(p);
            this.mensaje = "Datos eliminados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void cargarProducto(Producto p){
        categoria.setIdCategoria(p.getIdCategoria().getIdCategoria());
        this.producto=p;
    }
}
