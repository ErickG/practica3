/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Entity.Categoria;
import ejb.CategoriaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author erick.gomezusam
 */
@Named(value = "categoriaController")
@ManagedBean
@SessionScoped
public class CategoriaController implements Serializable{

    @EJB
    private CategoriaFacadeLocal categoriaFacade;
    private List<Categoria> listaCategoria;
    private Categoria categoria;
    String mensaje="";

    public List<Categoria> getListaCategoria() {
        this.listaCategoria = this.categoriaFacade.findAll();
        return listaCategoria;
    }

    public void setListaCategoria(List<Categoria> listaCategoria) {
        this.listaCategoria = listaCategoria;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
    @PostConstruct
    public void init(){
        this.categoria = new Categoria();
    }
     public void crear(){
        try {
            this.categoriaFacade.create(categoria);
            this.mensaje = "Datos creados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
     public void editar(){
        try {
            this.categoriaFacade.edit(categoria);
            this.mensaje = "Datos editados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
     public void eliminar(){
        try {
            this.categoriaFacade.remove(categoria);
            this.mensaje = "Datos eliminados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
     public void cargarCategoria(Categoria c){
         this.categoria = c;
     }
}
