/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Entity.Cliente;
import Entity.Factura;
import ejb.FacturaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author erick.gomezusam
 */
@Named(value = "facturaController")
@ManagedBean
@SessionScoped
public class FacturaController implements Serializable{

    @EJB
    private FacturaFacadeLocal facturaFacade;
    private List<Factura> listaFactura;
    private Factura factura;
    private Cliente cliente;
    String mensaje="";

    public List<Factura> getListaFactura() {
        this.listaFactura=facturaFacade.findAll();
        return listaFactura;
    }

    public void setListaFactura(List<Factura> listaFactura) {
        this.listaFactura = listaFactura;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    
    @PostConstruct
    public void init(){
        this.factura=new Factura();
        this.cliente=new Cliente();
    }
    public void crear(){
        try {
            this.factura.setIdCliente(cliente);
            this.facturaFacade.create(factura);
            this.mensaje = "Datos creados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editar(){
        try {
            this.factura.setIdCliente(cliente);
            this.facturaFacade.edit(factura);
            this.mensaje = "Datos editados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void eliminar(Factura f){
        try {
            this.facturaFacade.remove(f);
            this.mensaje = "Datos creados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void cargarFactura(Factura f){
        cliente.setIdCliente(f.getIdCliente().getIdCliente());
        this.factura=f;
    }
}
