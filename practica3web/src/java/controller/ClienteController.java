/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Entity.Cliente;
import ejb.ClienteFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author erick.gomezusam
 */
@Named(value = "clienteController")
@ManagedBean
@SessionScoped
public class ClienteController implements Serializable {

    @EJB
    private ClienteFacadeLocal clienteFacade;
    private List<Cliente> listaCliente;
    private Cliente cliente;
    String mensaje="";

    public List<Cliente> getListaCliente() {
        this.listaCliente = this.clienteFacade.findAll();
        return listaCliente;
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    @PostConstruct
    public void init(){
        this.cliente = new Cliente();
    }
    public void crear(){
        try {
            this.clienteFacade.create(cliente);
            this.mensaje = "Datos creados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editar(){
        try {
            this.clienteFacade.edit(cliente);
            this.mensaje = "Datos editados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void eliminar(Cliente c){
        try {
            this.clienteFacade.remove(c);
            this.mensaje = "Datos eliminados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
     public void cargarSoldado(Cliente c) {
        this.cliente = c;
    }
}
