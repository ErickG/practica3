/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Entity.Detalle;
import Entity.Factura;
import Entity.Producto;
import ejb.DetalleFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author erick.gomezusam
 */
@Named(value = "detalleController")
@SessionScoped
public class DetalleController implements Serializable {

    @EJB
    private DetalleFacadeLocal detalleFacade;
    private List<Detalle> listaDeetalle;
    private Detalle detalle;
    private Factura factura;
    private Producto producto;
    String mensaje="";

    public List<Detalle> getListaDeetalle() {
        this.listaDeetalle=detalleFacade.findAll();
        return listaDeetalle;
    }

    public void setListaDeetalle(List<Detalle> listaDeetalle) {
        this.listaDeetalle = listaDeetalle;
    }

    public Detalle getDetalle() {
        return detalle;
    }

    public void setDetalle(Detalle detalle) {
        this.detalle = detalle;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
    @PostConstruct
    public void init(){
        this.detalle=new Detalle();
        this.factura=new Factura();
        this.producto=new Producto();
    }
    public void crear(){
        try {
            this.detalle.setIdFactura(factura);
            this.detalle.setIdProducto(producto);
            this.detalleFacade.create(detalle);
            this.mensaje = "Datos creados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editar(){
        try {
            this.detalle.setIdFactura(factura);
            this.detalle.setIdProducto(producto);
            this.detalleFacade.edit(detalle);
            this.mensaje = "Datos editados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void eliminar(Detalle d){
        try {
            this.detalle.setIdFactura(factura);
            this.detalle.setIdProducto(producto);
            this.detalleFacade.remove(d);
            this.mensaje = "Datos creados exitosamente";
        } catch (Exception e) {
            this.mensaje = "Error: "+e.getMessage();
            e.printStackTrace();;
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void cargarDetalle(Detalle d){
        factura.setNumFactura(d.getIdFactura().getNumFactura());
        producto.setId_Producto(d.getIdProducto().getId_Producto());
        this.detalle=d;
    }
}
